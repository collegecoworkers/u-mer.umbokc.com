<?php

class DbmsDB {

	public static $data = [];
	public static $filename;
	public static $running = false;
	public static $active_table = '';

	public static function arr2Hex($arr){
		$string = json_encode($arr);
		$hex = '';
		for ($i=0; $i<strlen($string); $i++){
			$ord = ord($string[$i]);
			$hexCode = dechex($ord);
			$hex .= substr('0'.$hexCode, -2);
		}
		return strToUpper($hex);
	}

	public static function hex2Arr($hex){
		$string='';
		for ($i=0; $i < strlen($hex)-1; $i+=2){
			$string .= chr(hexdec($hex[$i].$hex[$i+1]));
		}
		return json_decode($string, true);
	}

	public static function run($file_name){
		self::$filename = $file_name;

		if (!file_exists(self::$filename) or self::$running)
			return;
		self::$running = true;

		$content = file_get_contents(self::$filename);

		if ($content == '') return;

		self::$data = self::hex2Arr($content);

	}

	public static function save(){
		file_put_contents(self::$filename, self::arr2Hex(self::$data));
		return "Saved\n";
	}

	public static function close(){
		return self::save();
	}



	public static function list(){
		$result = "";
		foreach(self::$data as $key => $v){
			$result .= $key . "\n";
		}
		return $result;
	}

	public static function create($key){
		self::$data[$key] = [];
		return "Table '$key' created\n";
	}

	public static function use($key){
		if(array_key_exists($key, self::$data)) {
			self::$active_table = $key;
			return "Table '$key' is used\n";
		}
		return "Table '$key' not found\n";
	}

	public static function active(){
		if(self::$active_table == '')
			return "Not active table\n";
		else
			return self::$active_table . "\n";
	}

	public static function unactive(){
		self::$active_table = "";
		return self::$active_table;
	}

	public static function delete($key){
		if(array_key_exists($key, self::$data)) {
			unset(self::$data[$key]);
			if (self::$active_table == $key)
				self::$active_table = '';
			return "Table '$key' removed\n";
		}
		return "Table '$key' not found\n";
	}

	public static function all(){
		if (self::$active_table == '') return "Not active table\n";
		$s = "";
		foreach (self::$data[self::$active_table] as $k => $v) {
			$s .= "$k -> $v\n";
		}
		return $s;
	}

	public static function add($key, $value){
		if (self::$active_table == '') return "Not active table\n";
		self::$data[self::$active_table][$key] = $value;
		return "$key -> $value\n";
	}

	public static function get($key){
		if (self::$active_table == '') return "Not active table\n";
		if (array_key_exists($key, self::$data[self::$active_table])){
			return self::$data[self::$active_table][$key] . "\n";
		}
		return "Key '$key' not found\n";
	}

	public static function rem($key){
		if (self::$active_table == '') return "Not active table\n";

		if (array_key_exists($key, self::$data[self::$active_table])){
			unset(self::$data[self::$active_table][$key]);
			return "Key '$key' removed\n";
		}
		return "Key '$key' not found\n";
	}

}

<?php

require_once 'lexer.php';
require_once 'token.php';
require_once 'db.php';

class DbmsParser{

	private $tokens;
	private $EOF;
	private $pos;
	private $size;

	public function __construct($line){
		$this->tokens = (new DbmsLexer($line))->tokenize();
		$this->EOF = new DbmsToken(DbmsTokenType::$EOF, "EOF");
		$this->pos = 0;
		$this->size = count($this->tokens);
		// dbg($this->tokens);
	}

	public function execute(){
		return $this->statement();
	}

	private function statement(){
		$current = $this->get(0);
		if($this->match(DbmsTokenType::$KEYS['EXIT'])){
			echo(DbmsDB::save());
			echo("Buy\n");
			die();
		} elseif ($this->match(DbmsTokenType::$KEYS['CLEAR'])){
			system("clear");
			return '';
		}
		elseif ($this->match(DbmsTokenType::$KEYS['SAVE'])) return DbmsDB::save();

		elseif ($this->match(DbmsTokenType::$KEYS['LIST'])) return DbmsDB::list();
		elseif ($this->match(DbmsTokenType::$KEYS['CREATE'])) return DbmsDB::create($this->expression()->get_text());
		elseif ($this->match(DbmsTokenType::$KEYS['USE'])) return DbmsDB::use($this->expression()->get_text());
		elseif ($this->match(DbmsTokenType::$KEYS['ACTIVE'])) return DbmsDB::active();
		elseif ($this->match(DbmsTokenType::$KEYS['UNUSE'])) return DbmsDB::unactive();
		elseif ($this->match(DbmsTokenType::$KEYS['DEL'])) return DbmsDB::delete($this->expression()->get_text());

		elseif ($this->match(DbmsTokenType::$KEYS['ALL'])) return DbmsDB::all();
		elseif ($this->match(DbmsTokenType::$KEYS['ADD'])) return DbmsDB::add($this->expression()->get_text(), $this->expression()->get_text());
		elseif ($this->match(DbmsTokenType::$KEYS['GET'])) return DbmsDB::get($this->expression()->get_text());
		elseif ($this->match(DbmsTokenType::$KEYS['REM'])) return DbmsDB::rem($this->expression()->get_text());
	}

	private function expression(){
		$current = $this->get(0);

		if($this->match(DbmsTokenType::$TEXT) or $this->match(DbmsTokenType::$WORD)){
			return $current;
		}

		$this->err('Error parsing');
	}

	private function match($type) {
		$current = $this->get(0);

		if($type !== $current->get_type()) return false;
		$this->pos++;
		return true;
	}

	private function get($relPos=0)	{
		$position = $this->pos + $relPos;
		if($position >= $this->size) {
			return $this->EOF;
		}
		return $this->tokens[$position]; 
	}

	private function err($mess, $q = false){
		DbmsDB::save();
		echo $mess . "\n";
		if($q)
			die();
	}
}

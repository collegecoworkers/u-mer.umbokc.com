<?php

require_once 'token.php';

class DbmsLexer{

	private $pos;
	private $input;
	private $length;
	private $tokens;

	public function __construct($input){
		$this->pos = 0;
		$this->input = $input;
		$this->length = strlen($input);
		$this->tokens = [];
	}

	public function tokenize(){
		while ($this->pos < $this->length){
			$current = $this->peek(0);
			if ($this->is_literal($current)) $this->tokenize_word();
			elseif ($current == '\'') $this->tokenize_string();
			else $this->next();
		}
		return $this->tokens;
	}

	private function tokenize_word(){
		$current = $this->peek(0);
		$buff = '';

		while(true){
			if (!(is_numeric($current) or $this->is_literal($current)))
				break;
			$buff .= $current;
			$current = $this->next();
		}

		if (array_key_exists(strtoupper($buff), DbmsTokenType::$KEYS))
			$this->tokens[] = new DbmsToken(DbmsTokenType::$KEYS[strtoupper($buff)], $buff);
		else
			$this->tokens[] = new DbmsToken(DbmsTokenType::$WORD, $buff);
	}

	private function tokenize_string(){

		$this->next();
		$current = $this->peek(0);
		$buff = '';

		while (true) {

			if($current == '\\'){
				$current = $this->next();
				switch ($current) {
					case '\\': $current = $this->next(); $buff .= "\\"; continue(2);
					case '\'': $current = $this->next(); $buff .= "'"; continue(2);
				}
				$buff .= "\\";
				continue;
			}

			if($current == '\'') break;
			if ($current == "\0") $this->lexer_error('Reached end of command while parsing text string.');
			$buff .= $current;
			$current = $this->next();
		}

		$this->next();

		$this->tokens[] = new DbmsToken(DbmsTokenType::$TEXT, $buff);
	}

	private function next(){
		$this->pos += 1;
		return $this->peek(0);
	}

	private function is_literal($char){
		return preg_match('~\w+|\$~', $char);
	}

	private function peek($relPos=0){
		$position = $this->pos + $relPos;
		if($position >= $this->length) return "\0";
		return $this->input[$position];
	}
}

<?php

class DbmsTokenType{
	public static $KEYS = [
		'CREATE' => 'CREATE',
		'DEL' => 'DEL',
		'LIST' => 'LIST',
		'USE' => 'USE',
		'UNUSE' => 'UNUSE',
		'ACTIVE' => 'ACTIVE',

		'ADD' => 'ADD',
		'GET' => 'GET',
		'REM' => 'REM',
		'ALL' => 'ALL',

		'EXIT' => 'EXIT',
		'CLEAR' => 'CLEAR',
		'SAVE' => 'SAVE',
	];

	public static $WORD = 'WORD';
	public static $TEXT = 'TEXT';
	public static $NUMBER = 'NUMBER';
	public static $EOF = 'EOF';
}

class DbmsToken {

	private $type;
	private $text = '';

	function __construct($type, $text) {
		$this->type = $type;
		$this->text = $text;
	}

	function get_type() {
		return $this->type;
	}

	function set_type($type) {
		$this->type = $type;
	}

	function get_text() {
		return $this->text;
	}

	function __toString() {
		return $this->type . ' ' . $this->text;
	}
}
<?php

define('BASE_DIR', getcwd());
function dbg($s, $d = false){ echo "<pre>"; if($s) print_r($s); else var_dump($s); echo "\n"; echo "</pre>"; if($d) die; }
require_once 'dbms/dbms_parser.php';

class DB {
	// public static function get(){}
	public static function query($q){
		return (new DbmsParser($q))->execute();
	}

	public static function list(){
		return self::query('list');
	}

	public static function create($t){
		return self::query("create '$t'");
	}

	public static function get($t, $k){
		self::query("use '$t'");
		$res = self::query("get '$k'");
		self::query("unuse");
		return $res;
	}

	public static function add($t, $k, $v){
		self::query("use '$t'");
		$res = self::query("add '$k' '$v'");
		self::query("unuse");
		return $res;
	}

	public static function all($t){
		self::query("use '$t'");
		$res = self::query("all");
		self::query("unuse");
		return $res;
	}

	public static function del_of_key($t, $k){
		self::query("use '$t'");
		$res = self::query("rem '$k'");
		self::query("unuse");
		return $res;
	}

	public static function del($t){
		return self::query("del '$t'");
	}

}

DbmsDB::run(BASE_DIR . '/data.dat');

if (isset($_GET['deltbl'])) {
	DB::del($_GET['deltbl']);
	DbmsDB::close();
	header('Location: /');
}

if (isset($_POST['create'])) {
	$curent_table = $_POST['table'];

	DB::create($curent_table);

	DbmsDB::close();
	header('Location: /?table=' . $curent_table);
}

if (isset($_POST['editForm']) or isset($_POST['add'])) {
	$curent_table = $_POST['curent_table'];

	DB::add($curent_table, $_POST['key'], $_POST['value']);

	DbmsDB::close();
	header('Location: /?table=' . $curent_table);
}

$tables = array_filter(explode("\n", DB::list()));

$curent_table = isset($_GET['table']) ? $_GET['table'] : $tables[0];

if (isset($_GET['del'])) {
	DB::del_of_key($curent_table, $_GET['del']);
	DbmsDB::close();
	header('Location: /?table=' . $curent_table);
}

$data = [];
$val = array_filter(explode("\n", DB::all($curent_table)));
foreach ($val as $value) {
	$temp = explode(" -> ", $value);
	$data[$temp[0]] = $temp[1];
}

DbmsDB::close();

include('template/main.php');

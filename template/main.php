<!DOCTYPE HTML>
<html lang='ru' ea>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

	<title>Система управления базой данных</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css" />
	<link rel="stylesheet" type="text/css" href="assets/jquery/jquery-ui.css" />
	<link rel="stylesheet" href="http://cdn.uwebu.ru/u-emmet-attr/src/ea.min.css">

	<script src="http://cdn.uwebu.ru/u-emmet-attr/src/ea.min.js"></script>

</head>
<body ovx:h>
	<div id="pma_navigation">
		<div id="pma_navigation_resizer"></div>
		<div id="pma_navigation_content">
			<div id="pma_navigation_header" p='1em 0' ta:c>
				<h1>Система управления базой данных</h1>
			</div>
			<div id="pma_navigation_tree" class="list_container highlight" pl=1em>
				<h3>Таблицы</h3>
				<div id='pma_navigation_tree_content'>
					<ul>
						<?php foreach ($tables as $key): ?>
							<li>
								<?php if ($curent_table == $key): ?>
									<a td:u href='/?table=<?= $key ?>'><?= $key ?></a>
								<?php else: ?>
									<a href='/?table=<?= $key ?>'><?= $key ?></a>
								<?php endif ?>
								<a href="/?deltbl=<?= $key ?>"><i class="fa fa-close"></i></a>
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id='serverinfo' h=14px w=100%>
	</div>
	<div id="topmenucontainer" class="menucontainer">
		<ul id="topmenu"  class="resizable-menu">
			<li class="active"><a href="" no-click curent_table="<?= $curent_table ?>">Данные таблицы: <?= $curent_table ?></a></li>
		</ul>
		<div class="clearfloat"></div>
	</div>

	<div id="page_content">
		<div>

			<div class="edit-form" mt=2em mb=1em d:n>
				<form method="post" action="/">
					<table class="insertRowTable">
						<thead>
							<tr>
								<th>Ключ</th>
								<th>Значение</th>
							</tr>
						</thead>
						<tfoot><tr><th colspan="5" class="tblFooters right">
							<input type="submit" value="OK" name="editForm">
						</th></tr></tfoot>
						<tbody>
							<tr class="noclick even">
								<input type="hidden" name="curent_table">
								<input type="hidden" name="key">
								<td class="center" p='2em 2em' edit-key>
									<p>Ключ</p>
								</td>
								<td>
									<textarea name="value" class="char" rows="2" cols="40" tabindex="2" fz=16px edit-value>Значение</textarea>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

			<div class="results">
				<form>
					<div class="data" style="position: relative;" mt=2em mb=2em>
						<table id="table_results" class="ajax pma_table">
							<thead>
								<tr>
									<th class="draggable column_heading pointer marker" style="cursor: move;"><span>
										<a href="">key</a>
										</span>
									</th>
									<th class="draggable column_heading pointer marker" style="cursor: move;"><span>
										<a href="">value</a>
										</span>
									</th>
									<th colspan="2">
										<span>
											Действия
										</span>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 0; ?>
								<?php foreach ($data as $key => $value): ?>
									<tr class="<?= $i % 2 == 0 ? 'odd' : 'even' ?> wrap">
										<td class="right data click2"><span pl=1em pr=1em><?= $key ?></span></td>
										<td class="data grid_edit click2"><span pl=1em pr=1em><?= $value ?></span></td>
										<td class="center">
											<a edit="<?= $key ?>" value="<?= $value ?>" pl=1em pr=1em>
												Изменить
											</a>
										</td>
										<td class="center">
											<a href="?table=<?= $curent_table ?>&del=<?= $key ?>" pl=1em pr=1em>
												Удалить
											</a>
										</td>
									</tr>
								<?php ++$i ?>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</form>
			</div>

			<hr />
			<form method="post" >
				<fieldset>
					<legend>
						Добавить запись
					</legend>
					<input type="hidden" name="curent_table" value="<?= $curent_table ?>" />
					<div class="formelement">
						Ключ:
						<input type="text" name="key" maxlength="64" />
					</div>
					<div class="formelement">
						Значение:
						<input type="text" name="value" maxlength="64" />
					</div>
					<div class="clearfloat"></div>
				</fieldset>
				<fieldset class="tblFooters">
					<input type="submit" value="OK" name="add"/>
				</fieldset>
			</form>
			<hr />
			<form method="post" >
				<fieldset>
					<legend>
						Создать таблицу
					</legend>
					<div class="formelement">
						Имя:
						<input type="text" name="table" maxlength="64" />
					</div>
					<div class="clearfloat"></div>
				</fieldset>
				<fieldset class="tblFooters">
					<input type="submit" value="OK" name="create" />
				</fieldset>
			</form>
		</div>
	</div>

<script>
	var edit_form = document.querySelector('.edit-form');

	var edit_form_key = edit_form.querySelector('[edit-key] p');
	var edit_form_curent_table = edit_form.querySelector('input[name=curent_table]');
	var edit_form_key_input = edit_form.querySelector('input[name=key]');
	var edit_form_value = edit_form.querySelector('[edit-value]');

	var results = document.querySelector('.results');
	var elems = document.querySelectorAll('[edit]');

	edit_form_curent_table.value = '<?= $curent_table ?>';

	elems.forEach(function(el){
		console.log(1);
		el.onclick = function(e){

			key = el.getAttribute('edit');
			value = el.getAttribute('value');

			edit_form_key.textContent = key;
			edit_form_key_input.value = key;
			edit_form_value.value = value;

			edit_form.removeAttribute('d:n');
			results.setAttribute('d:n', '');

		};
	});
</script>

</body>
</html>
